ARG LICENSE_FINDER_VERSION=5.11.1

FROM licensefinder/license_finder:$LICENSE_FINDER_VERSION
MAINTAINER GitLab

ARG LICENSE_FINDER_VERSION
ENV LICENSE_FINDER_VERSION $LICENSE_FINDER_VERSION

# Install JDK 11
RUN cd /tmp && \
    wget --quiet --no-cookies https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.5%2B10/OpenJDK11U-jdk_x64_linux_hotspot_11.0.5_10.tar.gz -O jdk-11.tgz && \
    tar xf /tmp/jdk-11.tgz && \
    mv jdk-11.0.5+10 /usr/lib/jvm/adoptopen_jdk11 && \
    rm /tmp/jdk-11.tgz
RUN npm install npm-install-peers

# Don't let Rubygem fail with the numerous projects using PG or MySQL,
# install realpath, includes for python3, and pip for python3
# Install additional php packages for better composer package support
# Install .NET Core 2.2, 3.0 because it is not installed in the license_finder image (https://github.com/pivotal/LicenseFinder/pull/632).
RUN add-apt-repository ppa:ondrej/php -y && apt-get update -y && \
    apt-get install -y --no-install-recommends \
    libpq-dev libmysqlclient-dev realpath python3-dev python3-pip dotnet-sdk-2.2 dotnet-sdk-3.0 \
    php7.1-mbstring php7.1-intl php7.1-xml php7.1-soap -y && \
    rm -rf /var/lib/apt/lists/*

# Install setuptools, and fetch a recent version pip to be installed later on
RUN pip install --upgrade pip setuptools \
  && python3 -m pip install --upgrade pip setuptools

# Version of Python, defaults to Python 3.5
ARG LM_PYTHON_VERSION=3.5
ENV LM_PYTHON_VERSION $LM_PYTHON_VERSION
ENV LM_REPORT_VERSION ${LM_REPORT_VERSION:-1}

COPY test /test
COPY run.sh /
COPY . /opt/license-management/
RUN bash -lc "cd /opt/license-management && gem build *.gemspec && gem install *.gem"
ENTRYPOINT ["/run.sh"]

# frozen_string_literal: true

require 'pathname'
require 'yaml'
require 'license_finder'
require 'license/management/loggable'
require 'license/management/verifiable'
require 'license/management/repository'
require 'license/management/report'
require 'license/management/version'

# This applies a monkey patch to the JsonReport found in the `license_finder` gem.
LicenseFinder::JsonReport.prepend(License::Management::Report)

# This monkey patch can be removed once we upgrade to license_finder 5.9.2. Details [here](https://gitlab.com/gitlab-org/gitlab/issues/13748#note_235810786).
module LicenseFinder
  class Bundler < PackageManager
    def definition
      @definition ||=
        begin
          Dir.chdir(project_path.to_s) do
            ::Bundler::Definition.build(detected_package_path, lockfile_path, nil)
          end
        end
    end
  end
end

module License
  module Management
    def self.root
      Pathname.new(File.dirname(__FILE__)).join('../..')
    end
  end
end

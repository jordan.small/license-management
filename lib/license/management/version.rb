# frozen_string_literal: true

module License
  module Management
    VERSION = '2.2.3'
  end
end

# frozen_string_literal: true

module License
  module Management
    module Report
      class Base
        include Loggable
        include Verifiable

        attr_reader :dependencies, :repository

        def initialize(dependencies)
          @dependencies = dependencies.sort
          @repository = License::Management::Repository.new
        end

        def to_h
          raise NotImplementedError
        end

        private

        def paths_from(dependency)
          return [] unless dependency.respond_to?(:aggregate_paths)

          paths = dependency.aggregate_paths
          return [] if blank?(paths)

          paths.map { |x| x.gsub(Dir.pwd, '.') }
        end

        def description_for(dependency)
          present?(dependency.summary) ? dependency.summary : dependency.description
        end
      end
    end
  end
end

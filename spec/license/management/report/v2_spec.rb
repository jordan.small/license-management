RSpec.describe License::Management::Report::V2 do
  describe "#to_h" do
    {
      'AGPL-1.0' => 'AGPL-1.0',
      'AGPL-3.0' => 'AGPL-3.0',
      'Apache 2.0' => 'Apache-2.0',
      'Artistic-2.0' => 'Artistic-2.0',
      'BSD' => 'BSD-4-Clause',
      'CC0 1.0 Universal' => 'CC0-1.0',
      'CDDL-1.0' => 'CDDL-1.0',
      'CDDL-1.1' => 'CDDL-1.1',
      'EPL-1.0' => 'EPL-1.0',
      'EPL-2.0' => 'EPL-2.0',
      'GPLv2' => 'GPL-2.0',
      'GPLv3' => 'GPL-3.0',
      'ISC' => 'ISC',
      'LGPL' => 'LGPL-3.0-only',
      'LGPL-2.1' => 'LGPL-2.1',
      'MIT' => 'MIT',
      'Mozilla Public License 2.0' => 'MPL-2.0',
      'MS-PL' => 'MS-PL',
      'MS-RL' => 'MS-RL',
      'New BSD' => 'BSD-3-Clause',
      'Python Software Foundation License' => 'Python-2.0',
      'ruby' => 'Ruby',
      'Simplified BSD' => 'BSD-2-Clause',
      'WTFPL' => 'WTFPL',
      'Zlib' => 'Zlib'
    }.each do |old_name, spdx_id|
      context "when mapping the legacy license name #{old_name}" do
        subject { described_class.new([dependency]) }

        let(:license) { LicenseFinder::License.new(short_name: old_name, matcher: LicenseFinder::License::NoneMatcher.new, url: nil) }
        let(:dependency) { instance_double(LicenseFinder::Package, name: 'x', summary: '', description: '', homepage: '', licenses: [license]).as_null_object }
        let(:result) { subject.to_h }

        specify { expect(result[:version]).to eq('2.0') }
        specify { expect(result[:licenses].count).to be(1) }
        specify { expect(result[:licenses][0]['id']).to eq(spdx_id) }
      end
    end

    context "when choosing an appropriate url for a license" do
      subject { described_class.new([dependency]) }
      let(:license) { LicenseFinder::License.new(short_name: 'MIT', matcher: LicenseFinder::License::NoneMatcher.new, url: nil) }
      let(:dependency) { instance_double(LicenseFinder::Package, name: 'x', summary: '', description: '', homepage: '', licenses: [license]).as_null_object }

      specify { expect(subject.to_h[:licenses][0]['url']).to eql('https://opensource.org/licenses/MIT') }
    end
  end
end

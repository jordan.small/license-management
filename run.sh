#!/bin/bash -l

set -e

usage="
$(basename "$0") [-h]

$(basename "$0") analyze PROJECT_PATH

$(basename "$0") test PROJECT_NAME RESULTS_NAME PROJECT_REF

where:
  -h  show this help text
  PROJECT_PATH = the path of the project to be analyzed for licenses usage.

  Set SETUP_CMD to skip auto-detection and use the specified command to install project dependencies."

if [ $# -eq 0 ] ; then
  echo "$usage"
  exit 1
fi

while getopts 'h' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

COMMAND=$1
shift

# "/run.sh" as a command means the user want the "analyze" command.
if [ "$COMMAND" = "/run.sh" ] ; then
  COMMAND="analyze"
fi
if [ "$COMMAND" = "analyse" ] ; then
  COMMAND="analyze"
fi

# "/test/test.sh" as a command means the user want the "test" command.
if [ "$COMMAND" = "/test/test.sh" ] ; then
  COMMAND="test"
fi

# Check number of arguments

if [ "$COMMAND" = "analyze" -a $# -ne 1 ] ; then
  echo "$usage"
  exit 1
fi

if [ "$COMMAND" = "test" -a $# -ne 3 ] ; then
  echo "$usage"
  exit 1
fi

# Switch to Python 3 if requesting
case "$LM_PYTHON_VERSION" in
  "2"|"2.7")
    echo "using python $LM_PYTHON_VERSION"
    LICENSE_FINDER_CLI_OPTS="--python-version 2 $LICENSE_FINDER_CLI_OPTS"
    pip2 --version
    ;;

  "3"|"3.5")
    echo "switching to python $LM_PYTHON_VERSION"
    LICENSE_FINDER_CLI_OPTS="--python-version 3 $LICENSE_FINDER_CLI_OPTS"
    pip --version
    ;;

  *)
    echo "python version not supported: $LM_PYTHON_VERSION" >&2
    exit 1
    ;;
esac

# Run command
case "$COMMAND" in
  test)
    # Run integration tests.
    exec /test/test.sh $1 $2 $3
    ;;

  analyze)
    # Analyze project

    # Change current directory to the project path.
    APP_PATH=$1
    shift
    pushd $APP_PATH > /dev/null

    if [[ -z "${SETUP_CMD}" ]]; then
        # Before running license_finder, we need to install dependencies for the project.
        if test -f Gemfile ; then
          if test -n "$rvm_recommended_ruby" ; then
            # Install the Ruby version RVM found in the project files
            # This always end in the cryptic "bash: Searching: command not found" error but Ruby is installed
            # So we ignore the error.
            $($rvm_recommended_ruby) 2>/dev/null || true
            rvm use .
            bundler_version=$(grep -A1 "BUNDLED WITH" Gemfile.lock  | tail -n 1)
            gem install bundler -v "${bundler_version}" || gem install bundler
            # rvm pulls outdated gems
            # need this to update system bundler
            # We need to install the license_finder gem into this Ruby version too.
            gem install license_finder -v "$LICENSE_FINDER_VERSION"
            gem install --no-document /opt/license-management/*.gem
          fi

          # Ignore test and development dependencies.
          license_finder ignored_groups add development
          license_finder ignored_groups add test
          bundle install --jobs $(nproc)
          skip_prepare=true
        fi

        if test -f package.json; then
          # Check is npm is being used
          if [ -f package-lock.json ] && [ ! -f yarn.lock ] ; then
            echo "Installing npm packages with npm ci"
            npm ci
          fi

          # install via yarn
          if [ ! -d node_modules ]; then
            echo "Installing yarn packages"
            yarn install --ignore-engines --ignore-scripts
          fi
          skip_prepare=true
        fi


        if find . -name "*.go" -printf "found" -quit |grep found >/dev/null ; then
          if [[ ( ! -f glide.lock ) && ( ! -f vendor/manifest ) && (! -f Gopkg.lock ) && (! -f go.mod ) ]]; then
            echo "running go get"
            # Only install deps if not using glide, govendor or dep
            # Symlink the project into GOPATH to allow fetching dependencies.
            ln -sf `realpath $APP_PATH` /gopath/src/app
            pushd /gopath/src/app > /dev/null
            go get || true
            skip_prepare=true
          fi
        fi

        if test -f pom.xml ; then
          # Install Java Maven dependencies.
          if [[ ${LM_JAVA_VERSION} = "11" ]]; then
            JAVA_HOME=/usr/lib/jvm/adoptopen_jdk11
          elif [[ ${LM_JAVA_VERSION} = "8" ]]; then
            JAVA_HOME=/usr/lib/jvm/oracle_jdk8
          fi
          mvn install ${MAVEN_CLI_OPTS:--DskipTests}
          skip_prepare=true
        fi

        if test -f build.gradle ; then
          if [[ ${LM_JAVA_VERSION} = "11" ]]; then
            JAVA_HOME=/usr/lib/jvm/adoptopen_jdk11
          elif [[ ${LM_JAVA_VERSION} = "8" ]]; then
            JAVA_HOME=/usr/lib/jvm/oracle_jdk8
          fi
          gradle build
          skip_prepare=true
        fi
        if [[ $(ls ./*.sln 2> /dev/null) ]]; then
          LICENSE_FINDER_CLI_OPTS="--recursive $LICENSE_FINDER_CLI_OPTS"
        fi
    else
        echo "Running '${SETUP_CMD[@]}' to install project dependencies..."
        ${SETUP_CMD[@]}
        skip_prepare=true
    fi

    # Run License Finder.
    echo "Running license_finder $@ in $PWD"
    if [ "$skip_prepare" != true ]; then
        prepare="--prepare-no-fail"
    fi
    echo "Preparing JSON report..."
    license_management report ${prepare} --format=json --save=gl-license-management-report.json ${LICENSE_FINDER_CLI_OPTS}
    popd > /dev/null
    ;;

  *)
    # Unknown command
    echo "Unknown command: $COMMAND"
    echo "$usage"
    exit 1
esac
